### setup process

1. use vs code to open project folder
2. npm install
3. npm start

### how to configure

you can customize your configuration in file src/config.js under root folder,
you can add your initial cities and your own appid for https://openweathermap.org/

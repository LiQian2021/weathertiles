export default {
  appid: "305f03c6ae4024af075ceab3f0e6e65e",
  initialCities: [
    { cityName: "Singapore", interval: 3 },
    { cityName: "Tokyo", interval: 5 },
    { cityName: "New York", interval: 10 },
  ],
};

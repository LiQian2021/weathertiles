import "./App.css";
import TileList from "./Components/TileList";
import WeatherTile from "./Components/WeatherTile";

function App() {
  return <TileList />;
}

export default App;

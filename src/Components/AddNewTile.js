import React, { useState } from "react";

export default function AddNewTile({ addNewCity }) {
  const [cityName, setCityName] = useState("");
  const [interval, setInterval] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    if (isNaN(interval) || interval < 1) {
      alert("interval must be a number and not less than 1");
      return;
    }
    addNewCity({ cityName: cityName, interval: interval });
  };

  return (
    <div
      style={{
        justifyContent: "center",
        border: "solid 2px",
        padding: "5px",
        borderRadius: "10px",
        margin: "30px",
        background: "#F08080",
        height: "150px",
      }}
    >
      <p>Add New City</p>
      <form onSubmit={handleSubmit}>
        <label style={{ display: "block", margin: "3px" }}>
          City Name:&nbsp;
          <input
            type="text"
            name="cityname"
            value={cityName}
            onChange={(e) => {
              setCityName(e.target.value);
            }}
          />
        </label>
        <label style={{ display: "block", margin: "3px" }}>
          Interval(minutes):&nbsp;
          <input
            style={{ width: "50px" }}
            type="text"
            name="interval"
            value={interval}
            onChange={(e) => {
              setInterval(e.target.value);
            }}
          />
        </label>
        <input
          type="submit"
          value="Confirm"
          style={{ borderRadius: "3px", background: "#FFFFE0" }}
        />
      </form>
    </div>
  );
}

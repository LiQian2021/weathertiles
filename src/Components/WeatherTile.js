import React, { useState, useEffect } from "react";
import config from "../config";

function WeatherTile({ city, interval, removeCity }) {
  const [weatherInfo, setWeatherInfo] = useState({});
  const [displayRemove, setdisplayRemove] = useState(false);

  useEffect(() => {
    const updateWeather = async () => {
      console.log("call updateWeather");
      try {
        const newWeatherResult = await fetch(
          `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${config.appid}&units=metric`
        );
        // console.log(newWeatherResult);
        const newWeather = await newWeatherResult.json();
        if (newWeather.cod === "404") {
          alert("city not found");
          removeCity(city);
          return;
        } else if (newWeather.cod === "409") {
          alert("too many requests, account got blocked by server");
          return;
        }
        setWeatherInfo(newWeather);
      } catch (ex) {
        console.log(ex);
        alert("error happened during updating weather");
      }
    };
    updateWeather();
    const periodicalUpdate = setInterval(() => {
      updateWeather();
    }, interval * 1000 * 60); //interval is calculated in minutes
    return () => clearInterval(periodicalUpdate);
  }, []);

  return (
    <div
      style={{
        width: "150px",
        textAlign: "center",
        display: "inline-block",
        margin: "30px",
      }}
      onMouseEnter={(e) => {
        setdisplayRemove(true);
      }}
      onMouseLeave={(e) => {
        setdisplayRemove(false);
      }}
    >
      <div
        style={{
          width: "100%",
          border: "solid 2px",
          borderRadius: "10px",
          overflow: "hidden",
        }}
      >
        <div style={{ background: "#00FFFF" }}>{city}</div>
        {weatherInfo.weather ? (
          <div>
            <div style={{ background: "#FF7F50" }}>
              <div>{weatherInfo && weatherInfo.weather[0].main}</div>
              <div>
                <img
                  src={`https://openweathermap.org/img/wn/${weatherInfo.weather[0].icon}@2x.png`}
                  alt="weatherIcon"
                />
              </div>
            </div>
            <div style={{ background: "#FFFAF0", overflow: "auto" }}>
              <p>temperature:{weatherInfo.main.temp}°C</p>
              <p>minimum temperature:{weatherInfo.main.temp_min}°C</p>
              <p>maximum temperature:{weatherInfo.main.temp_max}°C</p>
              <p>humidity:{weatherInfo.main.humidity}</p>
            </div>
          </div>
        ) : (
          <div>No Weather Info</div>
        )}
      </div>
      {displayRemove ? (
        <button
          style={{
            borderRadius: "3px",
            background: "#FF4500",
            height: "1.5rem",
          }}
          onClick={(e) => {
            removeCity(city);
          }}
        >
          Remove
        </button>
      ) : null}
    </div>
  );
}

export default WeatherTile;

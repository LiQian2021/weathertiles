import React, { useState, useEffect } from "react";
import WeatherTile from "./WeatherTile";
import config from "../config";
import AddNewTile from "./AddNewTile";

function TileList() {
  const [cities, setCities] = useState(config.initialCities);
  useEffect(() => {
    console.log("cities changed ", cities);
  }, [cities]);

  const handleRemoveCity = (city) => {
    setCities((previousCities) =>
      previousCities.filter((item) => item.cityName !== city)
    );
  };

  const handleAddCity = (newCity) => {
    setCities((previousCities) => {
      const newitem = {
        cityName: newCity.cityName,
        interval: newCity.interval,
      };
      return [...previousCities, newitem];
    });
  };

  console.log("cities=", cities);
  return (
    <div className="tiles-wrapper">
      {cities.map((item) => {
        return (
          <WeatherTile
            city={item.cityName}
            interval={item.interval}
            removeCity={handleRemoveCity}
          />
        );
      })}
      <AddNewTile addNewCity={handleAddCity} />
    </div>
  );
}

export default TileList;
